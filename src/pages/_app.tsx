import Head from 'next/head'
import { AppProps } from 'next/app'
import CssBaseline from '@material-ui/core/CssBaseline'

import 'react-toastify/dist/ReactToastify.css'

export default function App(props: AppProps): JSX.Element {
  const { Component, pageProps } = props
  return (
    <div>
      <Head>
        <title>RSA Encrypt/Decrypt</title>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
      </Head>
      <CssBaseline />
      <Component {...pageProps} />
    </div>
  )
}
